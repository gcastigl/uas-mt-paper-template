# Master Thesis Latex Template #

This is a Latex template for the UAS master thesis according to the template.docx documentation.

### Compiling ###

pdflatex template.tex


### Bug reporting ###

In case you find a bug or want to suggest an improvement just create a pull request or feel fee to email me.